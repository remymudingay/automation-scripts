from setuptools import setup

setup(
    name='import2vault',
    version='0.1',
    py_modules=['import2vault'],
    install_requires=[
        'Click',
        'pyyaml',
        'hvac',
    ],
    entry_points='''
        [console_scripts]
        import2vault=import2vault:cli
    ''',
)
